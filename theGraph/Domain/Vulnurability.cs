﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace theGraph.Domain
{
    public class Vulnurability : GraphNode
    {
        private int significancy;

        public Vulnurability(string name, int value, int s) : base(name, value)
        {
            this.Significancy = s;
        }

        public int Significancy
        {
            get { return significancy; }
            set
            {
                this.significancy = value;
                RaisePropertyChanged(nameof(Significancy));
            }
        }
    }
}
