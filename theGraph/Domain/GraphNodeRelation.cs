﻿using QuickGraph;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace theGraph.Domain
{
    public delegate void RelationChangedEventHandler(GraphNodeRelation gnr);

    public class GraphNodeRelation : INotifyPropertyChanged, IEdge<GraphNode>
    {
        public GraphNodeRelation(GraphNode source, GraphNode target, int rel = 0)
        {
            this.Source = source;
            this.Target = target;
            this.Relation = rel;
        }

        private int relation;
        public GraphNode Source { get; set; }
        public GraphNode Target { get; set; }

        public bool CanBeSet
        {
            get
            {
                return !Source.Equals(Target);
            }
        }

        public int Relation
        {
            get
            {
                return relation;
            }
            set
            {
                this.relation = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Relation)));
                RelationChanged?.Invoke(this);
            }
        }

        public override string ToString() => $"{Source} -> {Target}: {Relation}";
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        public event RelationChangedEventHandler RelationChanged = delegate { };

        public override bool Equals(object obj)
        {
            if (obj == null || !(obj is GraphNodeRelation))
                return false;
            GraphNodeRelation gnr = obj as GraphNodeRelation;

            return (this.Target.Equals(gnr.Target) && this.Source.Equals(gnr.Source) || this.Target.Equals(gnr.Source) && this.Source.Equals(gnr.Target));
        }
    }

    public static class GraphExtensions
    {
        public static bool InEdge<T>(this IEdge<T> edge, T node) where T : GraphNode
        {
            return edge.Source.Equals(node) || edge.Target.Equals(node);
        }
    }
}
