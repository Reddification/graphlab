﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace theGraph.Domain
{
    public enum NodeType
    {
        Attacker=1,
        Countermeasure,
        Resource,
        Vulnurability,
    }

    public abstract class GraphNode:INotifyPropertyChanged
    {
        private int _value;
        private string _name = "";

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Name)));
            }
        }
        public int Value
        {
            get => _value;
            set
            {
                _value = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Value)));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public GraphNode()
        {
            this.Name = "";
            this.Value = 1;
        }

        public GraphNode(string n, int v)
        {
            this.Name = n;
            this.Value = v;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (obj is GraphNode)
                return obj.GetType().Equals(this.GetType()) && this.Name == (obj as GraphNode).Name;
            else return base.Equals(obj);
        }

        protected void RaisePropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        public override string ToString() => this.Name + ": " + this.Value;
    }
}
