﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using theGraph.Domain;

namespace theGraph.Viewmodels
{
    public delegate void GraphNodeAddedEventHandler(GraphNode newNode);

    public class InitialDataViewmodel : INotifyPropertyChanged
    {
        public InitialDataViewmodel()
        {
            Countermeasure = new Countermeasure("C1", 0);
            Resource = new Resource("R1", 0);
            Vulnurability = new Vulnurability("V1", 0, 0);

            this.AddGraphNodeCommand = new LambdaCommand<GraphNode>(this.AddGraphNode, n => this.CanAddNode(n));
        }

        private Countermeasure countermeasure;
        private Resource resource;
        private Vulnurability vulnurability;
        private ICommand addGraphNodeCommand;

        public event PropertyChangedEventHandler PropertyChanged;
        public event GraphNodeAddedEventHandler GraphNodeAdded = (n) => { };

        public ObservableCollection<Attacker> Attackers { get; set; } = new ObservableCollection<Attacker>();
        public ObservableCollection<Countermeasure> Countermeasures { get; set; } = new ObservableCollection<Countermeasure>();
        public ObservableCollection<Resource> Resources { get; set; } = new ObservableCollection<Resource>();
        public ObservableCollection<Vulnurability> Vulnurabilities { get; set; } = new ObservableCollection<Vulnurability>();

        public Vulnurability Vulnurability
        {
            get => vulnurability;
            set
            {
                vulnurability = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Vulnurability)));
            }
        }
        public Resource Resource
        {
            get => resource;
            set
            {
                resource = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Resource)));
            }
        }
        public Countermeasure Countermeasure
        {
            get => countermeasure;
            set
            {
                countermeasure = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Countermeasure)));
            }
        }

        public ICommand AddGraphNodeCommand { get => addGraphNodeCommand; set => addGraphNodeCommand = value; }

        public void AddGraphNode(GraphNode n)
        {
            GraphNode gNode = null;

            switch (n)
            {
                case Resource r:
                    gNode = new Resource(r.Name, r.Value);
                    this.Resources.Add((Resource)gNode);
                    break;
                case Attacker a:
                    gNode = new Attacker(a.Name, a.Value);
                    this.Attackers.Add(gNode as Attacker);
                    break;
                case Vulnurability v:
                    gNode = new Vulnurability(v.Name, v.Value, v.Significancy);
                    this.Vulnurabilities.Add(gNode as Vulnurability);
                    break;
                case Countermeasure c:
                    gNode = new Countermeasure(c.Name, c.Value);
                    this.Countermeasures.Add(gNode as Countermeasure);
                    break;
                default:
                    break;
            }

            this.GraphNodeAdded?.Invoke(gNode);
        }

        private bool CanAddNode(GraphNode n)
        {
            switch (n)
            {
                case Resource r:
                    return !this.Resources.Any(rsc => rsc.Name == n.Name);
                case Attacker a:
                    return !this.Attackers.Any(at => at.Name == a.Name);
                case Vulnurability v:
                    return !this.Vulnurabilities.Any(vln => vln.Name == v.Name);
                case Countermeasure c:
                    return !this.Countermeasures.Any(cm => cm.Name == c.Name);
                default:
                    return true;
            }
        }

    }

    public class LambdaCommand<T> : ICommand where T : class
    {
        private Action<T> action;
        private Func<T, bool> canExecute;
        public event EventHandler CanExecuteChanged;

        public LambdaCommand(Action<T> a, Func<T, bool> ce)
        {
            this.action = a;
            this.canExecute = ce;
            CanExecuteChanged?.Invoke(this, new EventArgs());
        }

        public bool CanExecute(object parameter)
        {
            return canExecute(parameter as T);
        }

        public void Execute(object parameter)
        {
            action(parameter as T);
        }
    }

    public class AddGraphNodeCommand : ICommand
    {
        private ObservableCollection<Attacker> attackers;
        private ObservableCollection<Countermeasure> countermeasures;
        private ObservableCollection<Resource> resources;
        private ObservableCollection<Vulnurability> vulnurabilities;

        public AddGraphNodeCommand(ObservableCollection<Attacker> a,
                                   ObservableCollection<Countermeasure> c,
                                   ObservableCollection<Resource> r,
                                   ObservableCollection<Vulnurability> v)
        {
            this.attackers = a;
            this.countermeasures = c;
            this.resources = r;
            this.vulnurabilities = v;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            GraphNode gn = (GraphNode)parameter;
            return string.IsNullOrWhiteSpace(gn.Name) && gn.Value > 0;
        }

        public void Execute(object parameter)
        {
            switch (parameter)
            {
                case Attacker a:
                    this.attackers.Add(a);
                    break;
                case Vulnurability v:
                    this.vulnurabilities.Add(v);
                    break;
                case Countermeasure c:
                    this.countermeasures.Add(c);
                    break;
                case Resource r:
                    this.resources.Add(r);
                    break;
                default:
                    break;
            }
        }
    }
}
