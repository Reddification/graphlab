﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using GraphSharp.Algorithms.Layout.Simple.Hierarchical;
using GraphSharp.Controls;
using QuickGraph;
using QuickGraph.Algorithms.Search;
using theGraph.Domain;
using WPFExtensions.Controls;
using Xceed.Wpf.Toolkit.Zoombox;

namespace theGraph.Viewmodels
{
    public class TheGraphLayout:GraphLayout<GraphNode, GraphNodeRelation, BidirectionalGraph<GraphNode, GraphNodeRelation>>
    {

    }

    public class AttackGraphViewmodel : GraphViewmodel
    {
        private GraphNode _sourceResource;
        private GraphNode _targetResource;

        public GraphNode SourceResource
        {
            get => _sourceResource;
            set
            {
                _sourceResource = value;
                RaisePropertyChanged(nameof(SourceResource));
                dropEdgeHighlight();
            }
        }

        public GraphNode TargetResource
        {
            get => _targetResource;
            set
            {
                _targetResource = value;
                RaisePropertyChanged(nameof(TargetResource));
                dropEdgeHighlight();
            }
        }

        private int countermeasureCost;
        private int totalProtection;
        private int minimalVulnurabilityRate;
        private readonly RelationsViewmodel rvm;
        private readonly InitialDataViewmodel idvm;

        public ICommand FindShortestPathCommand { get; set; }
        public ICommand RebuildGraphCommand { get; set; }
        public int TotalProtection
        {
            get => totalProtection;
            set
            {
                totalProtection = value;
                RaisePropertyChanged(nameof(TotalProtection));
            }
        }

        public int CountermeasureCost
        {
            get => countermeasureCost;
            set
            {
                countermeasureCost = value;
                RaisePropertyChanged(nameof(CountermeasureCost));
            }
        }

        public int MinimalVulnurabilityRate
        {
            get => minimalVulnurabilityRate;
            set
            {
                minimalVulnurabilityRate = value;
                RaisePropertyChanged(nameof(MinimalVulnurabilityRate));
            }
        }

        public AttackGraphViewmodel(Zoombox zoom, TheGraphLayout _gLayout, RelationsViewmodel rvm, InitialDataViewmodel idvm) : base(zoom, _gLayout)
        {
            this.FindShortestPathCommand = new SimpleCommand(findSP, canFindSP);
            this.RebuildGraphCommand = new ParametrizedCommand((b) => rebuildGraph(b));
            this.rvm = rvm;
            this.idvm = idvm;
        }

        private void rebuildGraph(object b)
        {
            bool applyCountermeasures = (bool)b;

            var newGraph = new BidirectionalGraph<GraphNode, GraphNodeRelation>();
            var appliedCountermeasures = new List<Countermeasure>();
            var eliminatedVulnurabilities = new List<Vulnurability>();
            for (int i = 0; i < idvm.Resources.Count; i++)
            {
                newGraph.AddVertex(idvm.Resources[i]);
            }

            //an attack edge exists if
            //1. Resources are related
            //2. Resources either have common vulnurability or their vulnurability are related
            //3. Vulnurabilities aren't mitigated with countermeasures
            //4. Vulnurability rate > than minimum rate
            List<GraphNodeRelation> attackEdges = new List<GraphNodeRelation>();
            foreach (var r2r in rvm.ResourcesAdjacency.Where(w => w.Relation > 0))
            {
                var r1vulnurabilities = rvm.Resource2VulnurabilityRelation.Where(r2v => r2v.Relation > 0 && r2v.Target.Value > MinimalVulnurabilityRate && r2v.Source.Equals(r2r.Source)).Select(r2v => r2v.Target);
                var r2vulnurabilities = rvm.Resource2VulnurabilityRelation.Where(r2v => r2v.Relation > 0 && r2v.Target.Value > MinimalVulnurabilityRate && r2v.Source.Equals(r2r.Target)).Select(r2v => r2v.Target);

                if (applyCountermeasures)
                {
                    appliedCountermeasures.AddRange(rvm.Countermeasures2VulnurabilityRelation.Where(c2v => c2v.Relation > 0 && r1vulnurabilities.Any(v => c2v.Target.Equals(v))).Select(s => s.Source).Cast<Countermeasure>());
                    eliminatedVulnurabilities.AddRange(r1vulnurabilities.Where(w => rvm.Countermeasures2VulnurabilityRelation.Any(c2v => c2v.Relation > 0 && c2v.Target.Equals(w))).Cast<Vulnurability>());
                    r1vulnurabilities = r1vulnurabilities.Where(w => !rvm.Countermeasures2VulnurabilityRelation.Any(c2v => c2v.Relation > 0 && c2v.Target.Equals(w)));

                    eliminatedVulnurabilities.AddRange(r2vulnurabilities.Where(w => rvm.Countermeasures2VulnurabilityRelation.Any(c2v => c2v.Relation > 0 && c2v.Target.Equals(w))).Cast<Vulnurability>());
                    appliedCountermeasures.AddRange(rvm.Countermeasures2VulnurabilityRelation.Where(c2v => c2v.Relation > 0 && r2vulnurabilities.Any(v => c2v.Target.Equals(v))).Select(s => s.Source).Cast<Countermeasure>());
                    r2vulnurabilities = r2vulnurabilities.Where(w => !rvm.Countermeasures2VulnurabilityRelation.Any(c2v => c2v.Relation > 0 && c2v.Target.Equals(w)));
                }

                bool haveCommonVulnurabilities = r1vulnurabilities.Intersect(r2vulnurabilities).Any();

                var vAdjacencies = rvm.VulnurabilitiesAdjacency.Where(w => w.Source.Value > MinimalVulnurabilityRate && w.Target.Value > MinimalVulnurabilityRate);
                if (applyCountermeasures)
                {
                    var a2 = rvm.Countermeasures2VulnurabilityRelation.Where(c2v => c2v.Relation > 0 && vAdjacencies.Any(v2v => c2v.Target.Equals(v2v.Source) || c2v.Target.Equals(v2v.Target)));
                    appliedCountermeasures.AddRange(a2.Select(s => s.Source).Cast<Countermeasure>());
                    eliminatedVulnurabilities.AddRange(a2.Select(s => s.Target).Cast<Vulnurability>());
                    vAdjacencies = vAdjacencies.Where(v2v => !rvm.Countermeasures2VulnurabilityRelation.Any(c2v => c2v.Relation > 0 && (c2v.Target.Equals(v2v.Source) || c2v.Target.Equals(v2v.Target))));
                }

                bool haveRelatedVulnurabilities = 
                    vAdjacencies.Any(v2v => v2v.Relation > 0 && 
                    (r1vulnurabilities.Any(v => v.Equals(v2v.Source)) && r2vulnurabilities.Any(v => v.Equals(v2v.Target)) || r1vulnurabilities.Any(v => v.Equals(v2v.Target)) && r2vulnurabilities.Any(v => v.Equals(v2v.Source))));

                if (haveRelatedVulnurabilities || haveCommonVulnurabilities)
                    attackEdges.Add(r2r);
            }

            foreach (var attackEdge in attackEdges)
            {
                OnRelationAdded(attackEdge);
                if (attackEdge.Relation > 0)
                    newGraph.AddEdge(attackEdge);
            }

            this.Graph = newGraph;
            RaisePropertyChanged(nameof(Graph));
            if (applyCountermeasures)
            {
                this.CountermeasureCost = appliedCountermeasures.Distinct().Sum(s => s.Value);
                this.TotalProtection = eliminatedVulnurabilities.Distinct().Sum(s => s.Significancy);
            }
            else
            {
                this.CountermeasureCost = 0;
                this.TotalProtection = 0;
            }
            updateGraphUI();
        }

        private void dropEdgeHighlight()
        {
            foreach (var edge in this.Graph.Edges)
                gLayout.RemoveHighlightFromEdge(edge);
        }

        public bool canFindSP() => true;

        public void findSP()
        {
            Queue<Queue<GraphNodeRelation>> queue = new Queue<Queue<GraphNodeRelation>>();
            foreach (var item in Graph.OutEdges(SourceResource).Union(Graph.InEdges(SourceResource)))
            {
                queue.Enqueue(new Queue<GraphNodeRelation>(new GraphNodeRelation[] { item }));
            }

            var shortestPath = BFS(TargetResource, queue, new List<GraphNodeRelation>());

            if (shortestPath != null)
                foreach (var edge in shortestPath)
                {
                    this.gLayout.HighlightEdge(edge, null);
                }
            else MessageBox.Show("Attack path doesn't exist");
        }

        //every queue is a chain
        private Queue<GraphNodeRelation> BFS(GraphNode t, Queue<Queue<GraphNodeRelation>> queue, List<GraphNodeRelation> visitedEdges)
        {
            while (queue.Any())
            {
                var chain = queue.Dequeue();

                if (chain.Last().InEdge(t))
                    return chain;

                visitedEdges.Add(chain.Last());

                var edges = Graph.InEdges(chain.Last().Target).Union(Graph.InEdges(chain.Last().Source))
                                 .Union(Graph.OutEdges(chain.Last().Target)).Union(Graph.OutEdges(chain.Last().Source))
                                 .Where(w => !visitedEdges.Any(a => a.Equals(w)) && !queue.Any(q => q.Any(c => c.Equals(w))));
                foreach (var e in edges)
                {
                    Queue<GraphNodeRelation> newChain = new Queue<GraphNodeRelation>(chain);
                    newChain.Enqueue(e);
                    queue.Enqueue(newChain);
                }
            }

            return null;
        }

        public override void OnRelationAdded(GraphNodeRelation rel)
        {
            //rel.RelationChanged += OnRelationChanged;
        }

        public override void OnGraphNodeAdded(GraphNode newNode)
        {
            if (newNode is Resource)
            {
                this.Graph.AddVertex(newNode);
                base.OnGraphNodeAdded(newNode);
            }
        }

        public override void OnRelationChanged(GraphNodeRelation rel)
        {
            if (rel.Source is Resource && rel.Target is Resource)
            {
                if (rel.Relation > 0)
                {
                    this.Graph.AddEdge(rel);
                }
                else
                {
                    this.Graph.RemoveEdge(rel);
                }
            }
            else if (rel.Source is Resource && rel.Target is Vulnurability || rel.Source is Vulnurability && rel.Target is Resource)
            {

            }
            else if (rel.Source is Vulnurability && rel.Target is Vulnurability)
            {
                //are there any 2 adjacent resources that are connected because their vulnurabilities connected?
                if (rel.Relation == 0)
                {
                    Graph.RemoveEdgeIf((r) => r.Equals(rel));
                }
                else
                {

                }
            }
            else if (rel.Source is Vulnurability && rel.Target is Countermeasure || rel.Source is Countermeasure && rel.Target is Vulnurability)
            {

            }
            base.OnRelationChanged(rel);
        }
    }

    public class SimpleCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        private readonly Action commandAction;
        private readonly Func<bool> canExecuteAction;

        public SimpleCommand(Action a, Func<bool> canExecuteAction)
        {
            this.commandAction = a;
            this.canExecuteAction = canExecuteAction;
        }

        public bool CanExecute(object parameter)
        {
            return canExecuteAction();
        }

        public void Execute(object parameter)
        {
            commandAction();
        }
    }

    public class ParametrizedCommand : ICommand
    {
        private readonly Action<object> a;

        public event EventHandler CanExecuteChanged;

        public ParametrizedCommand(Action<object> a)
        {
            this.a = a;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            a.Invoke(parameter);
        }
    }
}
