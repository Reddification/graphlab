﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using theGraph.Domain;

namespace theGraph.Viewmodels
{
    public delegate void RelationAddedEventHandler(GraphNodeRelation rel);

    public class RelationsViewmodel:INotifyPropertyChanged
    {
        private int totalCountermeasuresCost;
        private readonly InitialDataViewmodel idvm;

        public ObservableCollection<GraphNodeRelation> ResourcesAdjacency { get; set; } = new ObservableCollection<GraphNodeRelation>();
        public ObservableCollection<GraphNodeRelation> VulnurabilitiesAdjacency { get; set; } = new ObservableCollection<GraphNodeRelation>();

        public ObservableCollection<GraphNodeRelation> Countermeasures2VulnurabilityRelation { get; set; } = new ObservableCollection<GraphNodeRelation>();
        public ObservableCollection<GraphNodeRelation> Resource2VulnurabilityRelation { get; set; } = new ObservableCollection<GraphNodeRelation>();

        public event RelationAddedEventHandler RelationAdded = delegate { };
        public event PropertyChangedEventHandler PropertyChanged;

        public RelationsViewmodel(InitialDataViewmodel idvm)
        {
            this.idvm = idvm;
        }

        public void OnGraphNodeAdded(GraphNode newNode)
        {
            switch (newNode)
            {
                case Resource r:
                    {
                        for (int i = 0; i < idvm.Resources.Count; i++)
                            ResourcesAdjacency.Add(new GraphNodeRelation(idvm.Resources[i], r));

                        for (int i = 0; i < idvm.Vulnurabilities.Count; i++)
                            Resource2VulnurabilityRelation.Add(new GraphNodeRelation(r, idvm.Vulnurabilities[i]));
                    }
                    break;
                case Vulnurability v:
                    {
                        for (int i = 0; i < idvm.Vulnurabilities.Count; i++)
                            VulnurabilitiesAdjacency.Add(new GraphNodeRelation(idvm.Vulnurabilities[i], v));

                        for (int i = 0; i < idvm.Resources.Count; i++)
                            Resource2VulnurabilityRelation.Add(new GraphNodeRelation(idvm.Resources[i], v));

                        for (int i = 0; i < idvm.Countermeasures.Count; i++)
                            Countermeasures2VulnurabilityRelation.Add(new GraphNodeRelation(idvm.Countermeasures[i], v));
                    }
                    break;
                case Countermeasure c:
                    {
                        for (int i = 0; i < idvm.Vulnurabilities.Count; i++)
                            Countermeasures2VulnurabilityRelation.Add(new GraphNodeRelation(c, idvm.Vulnurabilities[i]));
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
