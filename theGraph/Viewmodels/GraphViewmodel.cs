﻿using GraphSharp.Algorithms.Layout.Simple.FDP;
using GraphSharp.Algorithms.Layout.Simple.Hierarchical;
using QuickGraph;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using theGraph.Domain;
using WPFExtensions.Controls;
using Xceed.Wpf.Toolkit.Zoombox;

namespace theGraph.Viewmodels
{
    public abstract class GraphViewmodel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected readonly TheGraphLayout gLayout;

        //protected readonly WPFExtensions.Controls.ZoomControl zoom;
        protected readonly Zoombox zoom;

        public BidirectionalGraph<GraphNode, GraphNodeRelation> Graph { get; set; }

        public GraphViewmodel(Zoombox zoom, TheGraphLayout _gLayout)
        {
            this.Graph = new BidirectionalGraph<GraphNode, GraphNodeRelation>();
            this.gLayout = _gLayout;
            //this.gLayout.LayoutParameters = new FreeFRLayoutParameters { IdealEdgeLength = 100 };
            this.gLayout.LayoutParameters = new KKLayoutParameters();
            //this.gLayout.LayoutParameters = new FRLayoutAlgorithm<GraphNode, GraphNodeRelation, BidirectionalGraph<GraphNode, GraphNodeRelation>>(Graph);
            this.zoom = zoom;
        }

        public virtual void OnRelationChanged(GraphNodeRelation rel)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Graph)));
            updateGraphUI();
        }

        protected void updateGraphUI()
        {
            //gLayout.Relayout();
            zoom.CenterContent();
            zoom.FillToBounds();
            zoom.UpdateLayout();
            //zoom.UpdateLayout();
        }

        public virtual void OnRelationAdded(GraphNodeRelation rel)
        {

        }

        public virtual void OnGraphNodeAdded(GraphNode n)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Graph)));
            updateGraphUI();
        }

        protected void RaisePropertyChanged(string propName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
