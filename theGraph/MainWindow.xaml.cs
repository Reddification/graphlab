﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using theGraph.Domain;
using theGraph.Viewmodels;

namespace theGraph
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private InitialDataViewmodel idvm = new InitialDataViewmodel();
        private RelationsViewmodel rvm;
        private AttackGraphViewmodel agvm;
        public MainWindow()
        {
            InitializeComponent();
            rvm = new RelationsViewmodel(idvm);
            agvm = new AttackGraphViewmodel(aZoom, agLayout, rvm, idvm);
            this.modelsTab.DataContext = idvm;
            this.attackGraphTab.DataContext = agvm;
            this.matriciesTab.DataContext = rvm;

            generateTestData();

            idvm.GraphNodeAdded += agvm.OnGraphNodeAdded;
            idvm.GraphNodeAdded += rvm.OnGraphNodeAdded;
            rvm.RelationAdded += agvm.OnRelationAdded;
        }

        private void generateTestData()
        {
            Random R = new Random((int)DateTime.Now.Ticks);
            int resourcesCount = R.Next(6, 6);
            int vulnCount = R.Next(4, 8);
            int cmeasuresCount = R.Next(2, 5);

            for (int i = 0; i < resourcesCount; i++)
                idvm.Resources.Add(new Resource("R" + (i+1), 1));

            for (int i = 0; i < vulnCount; i++)
                idvm.Vulnurabilities.Add(new Vulnurability("V" + (i+1), R.Next(0, 100), R.Next(0, 100)));

            for (int i = 0; i < cmeasuresCount; i++)
                idvm.Countermeasures.Add(new Countermeasure("C" + (i+1), R.Next(0, 100)));

            for (int i = 0; i < resourcesCount - 1; i++)
                for (int j = i+1; j < resourcesCount; j++)
                    rvm.ResourcesAdjacency.Add(new GraphNodeRelation(idvm.Resources[i], idvm.Resources[j], R.Next() % 2));

            for (int i = 0; i < vulnCount - 1; i++)
                for (int j = i + 1; j < vulnCount; j++)
                    rvm.VulnurabilitiesAdjacency.Add(new GraphNodeRelation(idvm.Vulnurabilities[i], idvm.Vulnurabilities[j], R.Next(0, 5) >= 4 ? 1 : 0));

            for (int i = 0; i < resourcesCount; i++)
                for (int j = 0; j < vulnCount; j++)
                    rvm.Resource2VulnurabilityRelation.Add(new GraphNodeRelation(idvm.Resources[i], idvm.Vulnurabilities[j], R.Next(0, 5) >=4 ? 1: 0));

            for (int i = 0; i < cmeasuresCount; i++)
                for (int j = 0; j < vulnCount; j++)
                    rvm.Countermeasures2VulnurabilityRelation.Add(new GraphNodeRelation(idvm.Countermeasures[i], idvm.Vulnurabilities[j], R.Next(0, 5) >=4 ? 1: 0));
        }
    }
}
